export const getLoading = state => state.isFetching;
export const getFeeds = state => state.feeds;
export const getNextPage = state => state.nextPage;
export const getHasMore = state => state.hasMore;
export const getFetchStatus = state => state.success;
export const getActionSheetShow = state => state.showActionSheet;
